-------------------------------------------------------------------------------
-- This file is part of libsparkcrypto.
--
-- Copyright (C) 2013, Reto Guadagnini
-- Copyright (C) 2013, Hochschule fuer Technik Rapperswil
-- All rights reserved.
--
-- Redistribution  and  use  in  source  and  binary  forms,  with  or  without
-- modification, are permitted provided that the following conditions are met:
--
--    * Redistributions of source code must retain the above copyright notice,
--      this list of conditions and the following disclaimer.
--
--    * Redistributions in binary form must reproduce the above copyright
--      notice, this list of conditions and the following disclaimer in the
--      documentation and/or other materials provided with the distribution.
--
--    * Neither the name of the author nor the names of its contributors may be
--      used to endorse or promote products derived from this software without
--      specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE  COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY  EXPRESS OR IMPLIED WARRANTIES,  INCLUDING, BUT NOT LIMITED  TO, THE
-- IMPLIED WARRANTIES OF  MERCHANTABILITY AND FITNESS FOR  A PARTICULAR PURPOSE
-- ARE  DISCLAIMED. IN  NO EVENT  SHALL  THE COPYRIGHT  HOLDER OR  CONTRIBUTORS
-- BE  LIABLE FOR  ANY  DIRECT, INDIRECT,  INCIDENTAL,  SPECIAL, EXEMPLARY,  OR
-- CONSEQUENTIAL  DAMAGES  (INCLUDING,  BUT  NOT  LIMITED  TO,  PROCUREMENT  OF
-- SUBSTITUTE GOODS  OR SERVICES; LOSS  OF USE,  DATA, OR PROFITS;  OR BUSINESS
-- INTERRUPTION)  HOWEVER CAUSED  AND ON  ANY THEORY  OF LIABILITY,  WHETHER IN
-- CONTRACT,  STRICT LIABILITY,  OR  TORT (INCLUDING  NEGLIGENCE OR  OTHERWISE)
-- ARISING IN ANY WAY  OUT OF THE USE OF THIS SOFTWARE, EVEN  IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
-------------------------------------------------------------------------------

with LSC.Types;

use type LSC.Types.Index;
use type LSC.Types.Word32;
use type LSC.Types.Word64;

--# inherit
--#    LSC.Debug,
--#    LSC.IO,
--#    LSC.Types;

package LSC.SHA3 is

   -- KECCAK[] context
   type Context_Type is private;

   -- Index for KECCAK[] block
   -- For SHAKE128, a block consists of 21 words with 64 bits (1344 bits)
   subtype Block_Index is Types.Index range 0 .. 20;

   -- KECCAK[] block
   subtype Block_Type is Types.Word64_Array_Type (Block_Index);

   -- KECCAK[] block size in bytes
   -- For SHAKE128, the block size is 168 bytes (1344 bits)
   Block_Size : constant Types.Word64 := 168;

   -- KECCAK[] block length in bytes
   subtype Block_Length_Type is Types.Word64 range 0 .. Block_Size - 1;

   ----------------------------------------------------------------------------

   Null_Block : constant Block_Type;

   ----------------------------------------------------------------------------

   -- Initialize KECCAK[] context.
   function Context_Init return Context_Type;

   -- Update KECCAK[] context @Ctx@ with message block @Block@.
   procedure Context_Update
     (Ctx        : in out Context_Type;
      Block      : in     Block_Type;
      Block_Last : in     Block_Index);
   --# derives Ctx from *, Block, Block_Last;

   -- Finalize KECCAK[] context using @Length@ bytes of the final message block
   -- @Block@ and put KECCAK[] in the squeezing phase.
   procedure Context_Finalize
     (Ctx        : in out Context_Type;
      Block      : in     Block_Type;
      Block_Last : in     Block_Index;
      Length     : in     Block_Length_Type;
      Padding    : in     Types.Byte);
   --# derives Ctx from *, Block, Block_Last, Length, Padding;

   -- Squeeze the next portion @Hash_Block@ of the hash value out of
   -- the KECCAK[] sponge construction with context @Ctx@.
   procedure Squeeze
     (Ctx        : in out Context_Type;
      Hash_Block :    out Block_Type;
      Block_Last : in     Block_Index);
   --# derives Ctx from * &
   --#         Hash_Block from Ctx, Block_Last;

private

   Null_Block : constant Block_Type := Block_Type'(Block_Index => 0);

   -- Sheet Index
   subtype X_Index is Types.Index range 0 .. 4;

   -- Lane Index
   subtype Y_Index is Types.Index range 0 .. 4;

   -- Lanes of the KECCAK-f state
   subtype Lane_Type is Types.Word64;

   -- Sheets of the KECCAK-f state
   type Sheet_Type is array (Y_Index) of Lane_Type;

   -- The KECCAK-f state is made out of sheets which are
   -- made out of lanes
   type State_Type is array (X_Index) of Sheet_Type;

   type Context_Type is record
      State : State_Type;
   end record;

end LSC.SHA3;
