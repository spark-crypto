-------------------------------------------------------------------------------
-- This file is part of libsparkcrypto.
--
-- Copyright (C) 2013, Reto Guadagnini
-- Copyright (C) 2013, Hochschule fuer Technik Rapperswil
-- All rights reserved.
--
-- Redistribution  and  use  in  source  and  binary  forms,  with  or  without
-- modification, are permitted provided that the following conditions are met:
--
--    * Redistributions of source code must retain the above copyright notice,
--      this list of conditions and the following disclaimer.
--
--    * Redistributions in binary form must reproduce the above copyright
--      notice, this list of conditions and the following disclaimer in the
--      documentation and/or other materials provided with the distribution.
--
--    * Neither the name of the author nor the names of its contributors may be
--      used to endorse or promote products derived from this software without
--      specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE  COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY  EXPRESS OR IMPLIED WARRANTIES,  INCLUDING, BUT NOT LIMITED  TO, THE
-- IMPLIED WARRANTIES OF  MERCHANTABILITY AND FITNESS FOR  A PARTICULAR PURPOSE
-- ARE  DISCLAIMED. IN  NO EVENT  SHALL  THE COPYRIGHT  HOLDER OR  CONTRIBUTORS
-- BE  LIABLE FOR  ANY  DIRECT, INDIRECT,  INCIDENTAL,  SPECIAL, EXEMPLARY,  OR
-- CONSEQUENTIAL  DAMAGES  (INCLUDING,  BUT  NOT  LIMITED  TO,  PROCUREMENT  OF
-- SUBSTITUTE GOODS  OR SERVICES; LOSS  OF USE,  DATA, OR PROFITS;  OR BUSINESS
-- INTERRUPTION)  HOWEVER CAUSED  AND ON  ANY THEORY  OF LIABILITY,  WHETHER IN
-- CONTRACT,  STRICT LIABILITY,  OR  TORT (INCLUDING  NEGLIGENCE OR  OTHERWISE)
-- ARISING IN ANY WAY  OUT OF THE USE OF THIS SOFTWARE, EVEN  IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
-------------------------------------------------------------------------------

with LSC.Debug;
with LSC.IO;
with LSC.Types;

use type LSC.Types.Byte;

package body LSC.SHA3 is

   -- Constants for the KECCAK-f[1600] function
   L   : constant := 6;
   W   : constant := 2 ** L;
   N_R : constant := 12 + 2 * L; -- Number of rounds

   type Plane_Type is array (X_Index) of Lane_Type;

   -- Number of the KECCAK-f[1600] rounds
   subtype Round_Nr_Type is Integer range 0 .. (N_R - 1);

   -- KECCAK-f round constants
   subtype RC_Array_Index is Round_Nr_Type;
   type RC_Array_Type is array (RC_Array_Index) of Types.Word64;
   RC : constant RC_Array_Type := RC_Array_Type'(
                                                 16#0000000000000001#,
                                                 16#0000000000008082#,
                                                 16#800000000000808A#,
                                                 16#8000000080008000#,
                                                 16#000000000000808B#,
                                                 16#0000000080000001#,
                                                 16#8000000080008081#,
                                                 16#8000000000008009#,
                                                 16#000000000000008A#,
                                                 16#0000000000000088#,
                                                 16#0000000080008009#,
                                                 16#000000008000000A#,
                                                 16#000000008000808B#,
                                                 16#800000000000008B#,
                                                 16#8000000000008089#,
                                                 16#8000000000008003#,
                                                 16#8000000000008002#,
                                                 16#8000000000000080#,
                                                 16#000000000000800A#,
                                                 16#800000008000000A#,
                                                 16#8000000080008081#,
                                                 16#8000000000008080#,
                                                 16#0000000080000001#,
                                                 16#8000000080008008#
                                                );

   ----------------------------------------------------------------------------

   -- Creates an empty instance of the KECCAK-f state
   procedure Create_Empty_State (A : out State_Type)
   --# derives A from ;
   is
   begin

      A := State_Type'
        (others => Sheet_Type'
           (others => 0));

   end Create_Empty_State;

   ----------------------------------------------------------------------------

   -- Print KECCAK-f state
   procedure Print_State (A : in State_Type)
   is
   begin

      for Y in Y_Index loop
         for X in X_Index loop
            IO.Print_Word64 (A (X)(Y));
            IO.Put (" ");
         end loop;
         IO.New_Line;
      end loop;

   end Print_State;

   ----------------------------------------------------------------------------

   -- The Chi mapping
   procedure Chi (A : in out State_Type)
   --# derives A from A;
   is
      A_New : State_Type;
      Lane  : Types.Word64;
   begin

      Create_Empty_State (A => A_New);

      for Y in Y_Index loop
         for X in X_Index loop

            Lane := A ((X + 1) mod (X_Index'Last + 1))(Y);

            Lane := not Lane;

            Lane := Lane and A ((X + 2) mod (X_Index'Last + 1))(Y);

            A_New (X)(Y) := Lane xor A (X)(Y);

         end loop;
      end loop;

      A := A_New;

   end Chi;

   ----------------------------------------------------------------------------

   -- The Theta mapping
   procedure Theta (A : in out State_Type)
   --# derives A from A;
   is
      A_New : State_Type;
      Lane  : Types.Word64;
      C, D  : Plane_Type := Plane_Type'(others => 0); -- Temporary state planes
   begin

      Create_Empty_State (A => A_New);

      for X in X_Index loop
         C (X) := A (X)(0);

         for Y in Y_Index range 1 .. 4 loop
            C (X) := C (X) xor A (X) (Y);
         end loop;

      end loop;

      for X in X_Index loop
         Lane := C ((X + 1) mod (X_Index'Last + 1));
         Lane := Types.ROTL (Lane, 1); -- One bit left rotation
         D (X) :=
           C (X_Index ((Integer (X) - Integer (1))
              mod (Integer (X_Index'Last + 1)))) xor Lane;

         for Y in Y_Index loop
            A_New (X)(Y) := A (X)(Y) xor D (X);
         end loop;

      end loop;

      A := A_New;

   end Theta;

   ----------------------------------------------------------------------------

   -- The Pi mapping
   procedure Pi (A : in out State_Type)
   --# derives A from A;
   --# post (for all X in X_Index =>
   --#    (for all Y in Y_Index =>
   --#       (A (Y) ((2 * X + 3 * Y)  mod (Y_Index'Last + 1)) = A~ (X) (Y))));
   is
      A_New : State_Type;
   begin

      Create_Empty_State (A => A_New);

      for X in X_Index loop
         --# assert
         --#   A = A~ and
         --#   (for all I in X_Index range X_Index'First .. X - 1 =>
         --#     (for all J in Y_Index =>
         --#       (A_New(J)((2*I+3*J) mod (Y_Index'Last + 1)) = A(I)(J))));
         for Y in Y_Index loop
            --# assert
            --#   A = A~ and
            --#   (for all J in Y_Index range Y_Index'First .. Y - 1 =>
            --#     (A_New(J)((2*X+3*J) mod (Y_Index'Last + 1)) = A(X)(J))) and
            --#   (for all I in X_Index range X_Index'First .. X - 1 =>
            --#     (for all J in Y_Index =>
            --#       (A_New(J)((2*I+3*J) mod (Y_Index'Last + 1)) = A(I)(J))));
            A_New (Y) ((2 * X + 3 * Y) mod (Y_Index'Last + 1)) := A (X) (Y);
            --# assert
            --#   A = A~ and
            --#   (for all J in Y_Index range Y_Index'First .. Y =>
            --#     (A_New(J)((2*X+3*J) mod (Y_Index'Last + 1)) = A(X)(J))) and
            --#   (for all I in X_Index range X_Index'First .. X - 1 =>
            --#     (for all J in Y_Index =>
            --#       (A_New(J)((2*I+3*J) mod (Y_Index'Last + 1)) = A(I)(J))));
         end loop;
      end loop;

      A := A_New;

   end Pi;

   ----------------------------------------------------------------------------

   -- The Rho mapping
   procedure Rho (A : in out State_Type)
   --# derives A from A;
   is
      A_New : State_Type;
      X     : X_Index := 1;
      Y     : Y_Index := 0;
      X_New : X_Index;
      Y_New : Y_Index;
   begin
      Create_Empty_State (A => A_New);

      A_New (0)(0) := A (0)(0);

      for T in Integer range 0 .. 23 loop

         A_New (X)(Y) := Types.ROTL (A (X)(Y),
                           (((T + 1) * (T + 2)) / 2) mod (W));

         X_New := Y mod (X_Index'Last + 1);
         Y_New := (2 * X + 3 * Y) mod (Y_Index'Last + 1);
         X := X_New;
         Y := Y_New;

      end loop;

      A := A_New;

   end Rho;

   ----------------------------------------------------------------------------

   -- The Iota mapping
   procedure Iota
     (A    : in out State_Type;
      RC_I : in     Types.Word64)
   --# derives A from A, RC_I;
   is
   begin
      A (0)(0) := A (0)(0) xor RC_I;
   end Iota;

   ----------------------------------------------------------------------------

   -- A KECCAK-f[1600] round
   procedure Round
     (A        : in out State_Type;
      Round_Nr : in     Round_Nr_Type)
   --# derives A from A, Round_Nr;
   is
   begin

      pragma Debug (Debug.Put_Line (""));
      pragma Debug (Debug.Put_Line
                    ("Round: " & Round_Nr_Type'Image (Round_Nr)));
      pragma Debug (Debug.Put_Line ("-----------------------------"
                    & "---------------------------------------------------"));
      pragma Debug (Print_State (A));

      Theta (A);

      pragma Debug (Debug.Put_Line ("After theta:"));
      pragma Debug (Print_State (A));

      Rho (A);

      pragma Debug (Debug.Put_Line ("After rho:"));
      pragma Debug (Print_State (A));

      Pi (A);

      pragma Debug (Debug.Put_Line ("After pi:"));
      pragma Debug (Print_State (A));

      Chi (A);

      pragma Debug (Debug.Put_Line ("After chi:"));
      pragma Debug (Print_State (A));

      Iota (A, RC (Round_Nr));

      pragma Debug (Debug.Put_Line ("After iota:"));
      pragma Debug (Print_State (A));

   end Round;

   ----------------------------------------------------------------------------

   function KECCAK_F1600 (S : in State_Type) return State_Type
   is
      A : State_Type;
   begin

      A := S;

      for I in Round_Nr_Type loop
         Round (A        => A,
                Round_Nr => I);
      end loop;

      return A;
   end KECCAK_F1600;

   ----------------------------------------------------------------------------

   procedure Block_Terminate
     (Block      : in out Block_Type;
      Block_Last : in     Block_Index;
      Length     : in     Block_Length_Type;
      Padding    : in     Types.Byte)
   --# derives Block from *, Block_Last, Length, Padding;
   is
      -- Position of the last word in the Block
      Last_Word_Pos : Block_Index;

      Word_Bytes : Types.Byte_Array64_Type;

      -- Position of the last byte in the last word
      Last_Byte_Pos : Types.Byte_Array64_Index;
   begin
      Last_Word_Pos := Block_Index (Length / 8);

      Last_Byte_Pos := Types.Byte_Array64_Index (Length mod 8);

      Word_Bytes := Types.Word64_To_Byte_Array64 (Block (Last_Word_Pos));

      Word_Bytes (Last_Byte_Pos) := Padding;

      for I in Types.Byte_Array64_Index range Last_Byte_Pos + 1 .. 7 loop
         Word_Bytes (I) := 0;
      end loop;

      Block (Last_Word_Pos) := Types.Byte_Array64_To_Word64 (Word_Bytes);

      for I in Block_Index range Last_Word_Pos + 1 .. Block_Last loop
         Block (I) := 0;
      end loop;

      Word_Bytes := Types.Word64_To_Byte_Array64 (Block (Block_Last));

      Word_Bytes (7) := Word_Bytes (7) or 16#80#;

      Block (Block_Last) := Types.Byte_Array64_To_Word64 (Word_Bytes);
   end Block_Terminate;

   ----------------------------------------------------------------------------

   function Context_Init return Context_Type
   is
      Ctx : Context_Type;
   begin
      Create_Empty_State (Ctx.State);
      return Ctx;
   end Context_Init;

   ----------------------------------------------------------------------------

   procedure Context_Update
     (Ctx        : in out Context_Type;
      Block      : in     Block_Type;
      Block_Last : in     Block_Index)
   is
   begin

      pragma Debug (Debug.Put_Line (""));
      pragma Debug (Debug.Put_Line ("Updating context with block:"));
      pragma Debug (Debug.Print_Word64_Array (Block, 0, 1, True));

      for X in X_Index loop
         for Y in Y_Index loop
            if X + 5 * Y <= Block_Last then -- equivalent to x + 5y < r/w
               Ctx.State (X) (Y) := Ctx.State (X) (Y) xor Block (X + 5 * Y);
            end if;
         end loop;
      end loop;

      Ctx.State := KECCAK_F1600 (Ctx.State);

   end Context_Update;

   ----------------------------------------------------------------------------

   procedure Context_Finalize
     (Ctx        : in out Context_Type;
      Block      : in     Block_Type;
      Block_Last : in     Block_Index;
      Length     : in     Block_Length_Type;
      Padding    : in     Types.Byte)
   is
      Padded_Block : Block_Type;
   begin

      pragma Debug (Debug.Put_Line (""));
      pragma Debug (Debug.Put_Line ("Finalizing context with block:"));
      pragma Debug (Debug.Print_Word64_Array (Block, 0, 1, True));

      Padded_Block := Block;

      Block_Terminate (Padded_Block, Block_Last, Length, Padding);

      Context_Update (Ctx, Padded_Block, Block_Last);

   end Context_Finalize;

   ----------------------------------------------------------------------------

   procedure Squeeze
     (Ctx        : in out Context_Type;
      Hash_Block :    out Block_Type;
      Block_Last : in     Block_Index)
   is
   begin
      Hash_Block := Null_Block;

      -- Extract Hash_Block from state
      for X in X_Index loop
         for Y in Y_Index loop

            if X + 5 * Y <= Block_Last then -- equivalent to x + 5y < r/w
               Hash_Block (X + 5 * Y) := Ctx.State (X) (Y);
            end if;

         end loop;
      end loop;

      pragma Debug (Debug.Put_Line (""));
      pragma Debug (Debug.Put_Line ("Squeezed block:"));
      pragma Debug (Debug.Print_Word64_Array (Hash_Block, 0, 1, True));

      -- Update state
      Ctx.State := KECCAK_F1600 (Ctx.State);

   end Squeeze;

end LSC.SHA3;
