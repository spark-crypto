with LSC.Bignum;

private package LSC.Bignum.Print
is
   procedure Print_Big_Int
     (Item    : LSC.Bignum.Big_Int;
      Columns : Natural);

end LSC.Bignum.Print;
